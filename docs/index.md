# Elastic Search 개요 <sup>[1](#footnote_1)</sup>

Elasticsearch는 Apache Lucene을 기반으로 구축된 분산형 RESTful 검색과 분석 엔진이다. 이를 통해 대량의 데이터를 거의 실시간으로 신속하게 저장, 검색 및 분석할 수 있다. Elasticsearch는 복잡한 검색 기능과 요구 사항이 있는 어플리케이션을 구동하는 기본 엔진/기술로 자주 사용된다.

<a name="footnote_1">1</a>: 이 페이지는 [Introduction to Elastic Search](https://blog.devgenius.io/introduction-to-elastic-search-1183daacf2e0)를 편역한 것이다.
